import React, { useEffect, useState } from "react";
import Chat from "./Chat";
import "./Chats.css";
import { database } from "./firebase";
import Skeleton from "@material-ui/lab/Skeleton";
import { useStateValue } from "./store/StateProvider";

const Chats = (props) => {
  const [{ userId }, dispatch] = useStateValue();
  const [chatList, setChatList] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    if (userId) {
      const unsub = database
        .collection("people")
        .doc(userId)
        .collection("chat")
        .orderBy("created", "desc")
        .onSnapshot((snapshot) => {
          setChatList(
            snapshot.docs.map((doc) => ({ id: doc.id, ...doc.data() }))
          );
          setLoading(false);
        });
      return () => {
        unsub();
      };
    }
  }, [userId]);

  return (
    <div className="chats">
      {loading ? (
        <>
          {Array(10)
            .fill(1)
            .map((_, key) => (
              <div key={key} className="chats__skelton">
                <Skeleton variant="rect" height={50} />
              </div>
            ))}
        </>
      ) : (
        chatList.map((chat, key) => (
          <Chat
            key={key}
            name={chat.name}
            chatId={chat.id}
            message={chat.recentMsg}
            timestamp={chat.created.toDate().toDateString()}
            profilePic={chat.image}
          />
        ))
      )}
      {/* 
      <Chat
        name="Sandra"
        message="Olaaa!"
        timestamp="3 days ago"
        profilePic="https://images.pexels.com/photos/415829/pexels-photo-415829.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
      />
      <Chat
        name="Natasha"
        message="Oops there is he is ..."
        timestamp="1 week ago"
        profilePic="https://images.pexels.com/photos/943084/pexels-photo-943084.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
      /> */}
    </div>
  );
};

export default Chats;
