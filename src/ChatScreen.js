import React, { useEffect, useState } from "react";
import "./ChatScreen.css";
import Avatar from "@material-ui/core/Avatar";
import { database } from "./firebase";
import { useParams } from "react-router-dom";
import { useStateValue } from "./store/StateProvider";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useHistory,
} from "react-router-dom";
import firebase from "firebase";
import Skeleton from "@material-ui/lab/Skeleton";

const ChatScreen = () => {
  const [{ userId, chatId }, dispatch] = useStateValue();
  const { person } = useParams();
  const [input, setInput] = useState("");
  const [messages, setMessages] = useState([]);
  const [loading, setLoading] = useState(true);
  const history = useHistory();

  useEffect(() => {
    if ((userId, chatId)) {
      const unsub = database
        .collection("people")
        .doc(userId)
        .collection("chat")
        .doc(chatId)
        .collection("chatroom")
        .orderBy("created", "asc")
        .onSnapshot((snapshot) => {
          setMessages(snapshot.docs.map((doc) => doc.data()));
          setLoading(false);
        });
      return () => {
        unsub();
      };
    } else {
      history.replace("/");
    }
  }, [userId, chatId]);

  const handleSend = (e) => {
    e.preventDefault();
    const timestamp = firebase.firestore.FieldValue.serverTimestamp();
    database
      .collection("people")
      .doc(userId)
      .collection("chat")
      .doc(chatId)
      .update({ recentMsg: input, created: timestamp });
    database
      .collection("people")
      .doc(userId)
      .collection("chat")
      .doc(chatId)
      .collection("chatroom")
      .add({
        message: input,
        created: timestamp,
      });

    setInput("");
  };
  return (
    <div className="chatScreen">
      <p className="chatScreen__timestamp">YOU MATCHED WITH {person}</p>
      {loading ? (
        <>
          {Array(10)
            .fill(1)
            .map((_, key) => (
              <div key={key} className="chats__skelton">
                <Skeleton variant="rect" height={50} />
              </div>
            ))}
        </>
      ) : (
        messages.map((message, key) => {
          const isFriend = !!message?.name?.length;
          return (
            <div key={key} className="chatScreen__message">
              {isFriend && (
                <Avatar
                  className="chatScreen__image"
                  alt={message.name}
                  src={message.image}
                />
              )}
              <p
                className={
                  isFriend ? "chatScreen__text" : "chatScreen__textUser"
                }
              >
                {message.message}
              </p>
            </div>
          );
        })
      )}
      <div className="chatScreen__input">
        <form className="chatScreen__inputForm" onSubmit={handleSend}>
          <input
            type="text"
            placeholder="type a message"
            className="chatScreen__inputField"
            value={input}
            onChange={(e) => setInput(e.target.value)}
          />
          <button type="submit" className="chatScreen__inputButton">
            SEND
          </button>
        </form>
      </div>
    </div>
  );
};

export default ChatScreen;
