import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyBiuKPo98dct6-IJXWqLKUsN4k9JcSNhks",
  authDomain: "tinder-clone-22e55.firebaseapp.com",
  projectId: "tinder-clone-22e55",
  storageBucket: "tinder-clone-22e55.appspot.com",
  messagingSenderId: "518171536383",
  appId: "1:518171536383:web:43e8df0805f3beda9955ca",
  measurementId: "G-HYKNPNN90G",
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const database = firebaseApp.firestore();

export { database };
