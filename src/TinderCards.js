import React, { useEffect, useState } from "react";
import "./TinderCards.css";
import TinderCard from "react-tinder-card";
import { database } from "./firebase";
import Skeleton from "@material-ui/lab/Skeleton";

const TinderCards = () => {
  const [people, setPeople] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const unsub = database
      .collection("people")
      .where("name", "!=", "mark zuckerberg")
      .onSnapshot((snapshot) => {
        setPeople(snapshot.docs.map((doc) => doc.data()));
        setLoading(false);
      });
    return () => {
      unsub();
    };
  }, []);
  return (
    <div className="tinderCards">
      <div className="tinderCards__container">
        {loading ? (
          <>
            <Skeleton
              className="swipe__skelton"
              variant="rect"
              width={210}
              height={118}
            />
          </>
        ) : (
          people.map((person, key) => (
            <TinderCard
              // onSwipe={onSwipe}
              // onCardLeftScreen={() => onCardLeftScreen("fooBar")}
              preventSwipe={["up", "down"]}
              className="swipe"
              key={key}
            >
              <div
                className="card"
                style={{ backgroundImage: `url(${person.url})` }}
              >
                <span className="card__name">{person.name}</span>
              </div>
            </TinderCard>
          ))
        )}
      </div>
    </div>
  );
};

export default TinderCards;
