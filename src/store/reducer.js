export const initialState = {
  userId: null,
  chatId: null,
};

function reducer(state, action) {
  let newState = state;
  switch (action.type) {
    case "SET_USER_ID":
      if (!action.userId) return newState;
      newState = {
        ...state,
        userId: action.userId,
      };
      return newState;
    case "SET_CHAT_ID":
      if (!action.chatId) return newState;
      newState = {
        ...state,
        chatId: action.chatId,
      };
      return newState;
    default:
      return state;
  }
}

export default reducer;
