import React from "react";
import "./Chat.css";
import Avatar from "@material-ui/core/Avatar";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useHistory,
} from "react-router-dom";
import { useStateValue } from "./store/StateProvider";

const Chat = (props) => {
  const { chatId, name, message, timestamp, profilePic } = props;
  const [_, dispatch] = useStateValue();
  const setChatId = (chatId) => {
    dispatch({
      type: "SET_CHAT_ID",
      chatId: chatId,
    });
  };
  return (
    <Link to={`/chat/${name}`} onClick={() => setChatId(chatId)}>
      <div className="chat">
        <Avatar className="chat__image" alt={name} src={profilePic} />
        <div className="chat__details">
          <h2>{name}</h2>
          <p>{message}</p>
        </div>
        <p className="chat__timestamp">{timestamp}</p>
      </div>
    </Link>
  );
};

export default Chat;
