import React from "react";
import "./Header.css";
import PersonIcon from "@material-ui/icons/Person";
import ForumIcon from "@material-ui/icons/Forum";
import IconButton from "@material-ui/core/IconButton";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useHistory,
} from "react-router-dom";

const Header = (props) => {
  const { backButton = "/" } = props;
  const history = useHistory();
  return (
    <div className="header">
      <IconButton onClick={() => history.replace(backButton)}>
        {backButton ? (
          <ArrowBackIosIcon fontSize="large" className="header__icon" />
        ) : (
          <PersonIcon fontSize="large" className="header__icon" />
        )}
      </IconButton>
      <Link to="/">
        <img
          className="header__logo"
          src="https://www.vectorico.com/?wpfilebase_thumbnail=1&fid=369&name=Tinder-Logo-253x300._vpsW8qoVAnFZ.thumb.png"
          alt=""
        />
      </Link>
      <Link to="/chat">
        <IconButton>
          <ForumIcon fontSize="large" className="header__icon" />
        </IconButton>
      </Link>
    </div>
  );
};

export default Header;
