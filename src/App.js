import React, { useEffect } from "react";
import "./App.css";
import Header from "./Header";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import TinderCards from "./TinderCards";
import SwipeButtons from "./SwipeButtons";
import Chats from "./Chats";
import ChatScreen from "./ChatScreen";
import { database } from "./firebase";
import { useStateValue } from "./store/StateProvider";

const App = () => {
  const [_, dispatch] = useStateValue();

  useEffect(() => {
    const unsub = database
      .collection("people")
      .where("name", "==", "mark zuckerberg")
      .onSnapshot((snapshot) => {
        if (!snapshot.empty) {
          dispatch({
            type: "SET_USER_ID",
            userId: snapshot.docs[0].id,
          });
        }
      });
    return () => {
      unsub();
    };
  }, []);

  return (
    <div className="app">
      <Router>
        <Switch>
          <Route path="/chat/:person">
            <Header backButton="/chat" />
            <ChatScreen />
          </Route>
          <Route path="/chat">
            <Header backButton="/" />
            <Chats />
          </Route>
          <Route path="/">
            <Header />
            <TinderCards />
            <SwipeButtons />
          </Route>
        </Switch>
      </Router>
    </div>
  );
};

export default App;
