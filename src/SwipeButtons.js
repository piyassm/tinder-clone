import React from "react";
import "./SwipeButtons.css";
import ReplayIcon from "@material-ui/icons/Replay";
import CloseIcon from "@material-ui/icons/Close";
import StarRateIcon from "@material-ui/icons/StarRate";
import FavoriteIcon from "@material-ui/icons/Favorite";
import FlashOnIcon from "@material-ui/icons/FlashOn";
import IconButton from "@material-ui/core/IconButton";

const SwipeButtons = () => {
  return (
    <div className="swipeButtons">
      <div className="swipeButtons__container">
        <IconButton className="swipeButtons__repeat">
          <ReplayIcon fontSize="large" className="swipeButtons__icon" />
        </IconButton>
        <IconButton className="swipeButtons__left">
          <CloseIcon fontSize="large" className="swipeButtons__icon" />
        </IconButton>
        <IconButton className="swipeButtons__star">
          <StarRateIcon fontSize="large" className="swipeButtons__icon" />
        </IconButton>
        <IconButton className="swipeButtons__right">
          <FavoriteIcon fontSize="large" className="swipeButtons__icon" />
        </IconButton>
        <IconButton className="swipeButtons__lightning">
          <FlashOnIcon fontSize="large" className="swipeButtons__icon" />
        </IconButton>
      </div>
    </div>
  );
};

export default SwipeButtons;
